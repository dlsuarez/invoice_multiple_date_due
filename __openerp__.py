# -*- coding: utf-8 -*-
###############################################################################
#
#    Odoo, Open Source Management Solution
#
#    Copyright (c) All rights reserved:
#        (c) 2015  
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses
#
###############################################################################
{
    'name': 'Invoice Multiple Due Date',
    'summary': 'Invoice Multiple Due Date',
    'version': '1.0',
    'description': """
Invoice Multiple Due Date.
==============================================

    """,
    'author': 'Anubia, Soluciones en la Nube, SL',
    'website': "http://www.anubia.es",
    'maintainer': 'Anubia, soluciones en la nube, SL',
    'contributors': [
        'Daniel Lago Suárez <dlagosuarez@gmail.com>'
    ],
    'website': 'https://gitlab.com/dlsuarez/invoice_multiple_due_date/',
    'license': 'AGPL-3',
    'category': 'Base',
    'depends': [
        'account',
    ],
    'external_dependencies': {
        'python': []
    },
    'data': [
        'security/request_security.xml',
        'views/account_invoice.xml',
        'views/account_invoice_templates.xml',
        'views/account_voucher.xml',
    ],
    'demo': [],
    'js': [],
    'css': [],
    'qweb': [],
    'images': [],
    'test': [],
    'installable': True
}
