# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
from datetime import datetime, date, time, timedelta
import logging

class account_voucher(models.Model):

    _inherit = 'account.voucher'
    _logger = logging.getLogger(__name__)

    # --------------------------- ENTITY  FIELDS ------------------------------

    date_due_id = fields.Many2one(
        string='Date due id',
        required=False,
        readonly=False,
        index=False,
        default=None,
        comodel_name='account.invoice.due.date',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False
    )
