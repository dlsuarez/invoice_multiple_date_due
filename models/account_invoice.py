# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
from datetime import datetime, date, time, timedelta
import logging

class account_invoice_due_date(models.Model):

    _name = 'account.invoice.due.date'
    _rec_name = 'date_due'
    _description = _('Due dates')
    _order = 'date_due asc'
    _logger = logging.getLogger(__name__)

    # --------------------------- ENTITY  FIELDS ------------------------------

    invoice = fields.Many2one(
        string='Invoice',
        required=False,
        readonly=False,
        index=False,
        default=None,
        comodel_name='account.invoice',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False,
    )

    date_due = fields.Date(
        string='Due date',
        required=False,
        readonly=False,
        index=False,
        default=lambda self: date.today(),
    )

    vouchers = fields.One2many(
        string='Vouchers',
        required=False,
        readonly=False,
        index=False,
        default=None,
        comodel_name='account.voucher',
        inverse_name='date_due_id',
        domain=[],
        context={},
        auto_join=False,
        limit=None
    )

    used = fields.Boolean(
        string='Used',
        required=False,
        readonly=False,
        index=False,
        default=False,
        compute=lambda self: self._compute_used(),
    )

    _sql_constraints = [
        ('unique_due_date_per_invoice',
         'unique(date_due, invoice)',
         _('An invoice cannot have several due dates equal!')),
    ]

    # ----------------------------- NEW METHODS -------------------------------

    @api.model
    def due_date_notification(self):
        for invoice in self.env['account.invoice'].search([
            ('type', '=', 'out_invoice'),
            ('state', '=', 'open')
        ]):
            if invoice.multi_date_due:
                for multi in invoice.multi_date_due:
                    if self._check_due_date_in_interval(multi, -5):
                        ctx = self.env.context.copy()
                        ctx.update({'invoice_number': invoice.number})
                        template_five_before = self.env.ref(
                            'invoice_multiple_date_due.'
                            'five_days_before_due_date_template'
                        )
                        if template_five_before:
                            self._send_mail_mail(
                                invoice.partner_id,
                                template_five_before.id,
                                ctx
                            )
                    if self._check_due_date_in_interval(multi, 3):
                        ctx = self.env.context.copy()
                        ctx.update({'invoice_number': invoice.number})
                        template_five_before = self.env.ref(
                            'invoice_multiple_date_due.'
                            'three_days_after_due_date_template'
                        )
                        if template_five_before:
                            self._send_mail_mail(
                                invoice.partner_id,
                                template_five_before.id,
                                ctx
                            )
                    if self._check_due_date_in_interval(multi, 15):
                        ctx = self.env.context.copy()
                        ctx.update({'invoice_number': invoice.number})
                        template_five_before = self.env.ref(
                            'invoice_multiple_date_due.'
                            'fifteen_days_after_due_date_template'
                        )
                        if template_five_before:
                            self._send_mail_mail(
                                invoice.partner_id,
                                template_five_before.id,
                                ctx
                            )

    @api.model
    def due_date_summary_notification(self):
        ctx = self.env.context.copy()
        summary_due_dates = self.env['account.invoice.due.date']
        summary_late_payments = self.env['account.invoice.due.date']
        for invoice in self.env['account.invoice'].search([
            ('type', '=', 'out_invoice'),
            ('state', '=', 'open')
        ]):
            if invoice.multi_date_due:
                for multi in invoice.multi_date_due:
                    if self._check_due_date_in_period(multi, -7):
                        summary_due_dates = summary_due_dates + multi
                    if self._check_after_due_date(multi):
                        summary_late_payments = summary_late_payments + multi
        ctx.update({
            'summary_due_dates':  sorted(
                summary_due_dates,
                key=lambda x: (x.date_due, x.invoice.number)
            )
        })
        template_summary_due_dates = self.env.ref(
            'invoice_multiple_date_due.'
            'summary_due_dates_template'
        )
        if template_summary_due_dates:
            self._send_mail_mail(
                invoice.user_id.partner_id,
                template_summary_due_dates.id,
                ctx
            )
        ctx.update({
            'summary_late_payments': sorted(
                summary_late_payments,
                key=lambda x: (x.date_due, x.invoice.number)
            )
        })
        template_summary_late_payments = self.env.ref(
            'invoice_multiple_date_due.'
            'summary_late_payments_template'
        )
        if template_summary_late_payments:
            self._send_mail_mail(
                invoice.user_id.partner_id,
                template_summary_late_payments.id,
                ctx
            )

    # -------------------------- AUXILIARY METHODS ----------------------------

    def _send_mail_mail(self, partner, template_id, ctx):
        mails_sent = False
        if partner and partner.email:
            mails_sent = self.pool.get('email.template').send_mail(
                self.env.cr,
                self.env.ref('base.user_root').id,
                template_id,
                partner.id,
                force_send=True,
                context=ctx)
        return mails_sent

    def _check_due_date_in_interval(self, date_due, interval_number):
        date_due_date = fields.Datetime.from_string(date_due.date_due)
        delta = datetime.today() - date_due_date
        interval = delta - timedelta(days=interval_number)
        if interval < timedelta(days=1) and interval > timedelta(days=0) and \
            date_due.used == False:
            return True
        else:
            return False

    def _check_due_date_in_period(self, date_due, period):
        date_due_date = fields.Datetime.from_string(date_due.date_due)
        delta = datetime.today() - date_due_date
        if delta > timedelta(days=period) \
            and delta < timedelta(days=0) and date_due.used == False:
            return True
        else:
            return False

    def _check_after_due_date(self, date_due):
        date_due_date = fields.Datetime.from_string(date_due.date_due)
        delta = datetime.today() - date_due_date
        if delta > timedelta(days=0) and date_due.used == False:
            return True
        else:
            return False

    # ----------------------- AUXILIARY FIELD METHODS -------------------------

    @api.multi
    @api.depends('vouchers.date_due_id')
    def _compute_used(self):
        for record in self:
            if self.env['account.voucher'].search(\
                [('date_due_id', '=', record.id)]):
                record.used = True
            else:
                record.used = False

class account_invoice(models.Model):

    _inherit = 'account.invoice'
    _logger = logging.getLogger(__name__)

    # --------------------------- ENTITY  FIELDS ------------------------------

    date_due = fields.Date(
        string='Due date',
        compute=lambda self: self._compute_date_due(),
        store=False,
    )

    multi_date_due = fields.One2many(
        string='Multiple due dates',
        required=False,
        readonly=False,
        index=False,
        default=[(0, 0, {
            'date_due': datetime.today().strftime("%Y-%m-%d"),
            'invoice': False,
        }), ],
        help=False,
        comodel_name='account.invoice.due.date',
        inverse_name='invoice',
        domain=[],
        context={},
        auto_join=False,
        limit=None,
    )

    has_multi_date_due = fields.Boolean(
        string='Has multi date due',
        required=False,
        readonly=False,
        index=False,
        default=False,
        compute=lambda self: self._compute_has_multi_date_due(),
        help=False,
    )

    # ----------------------- AUXILIARY FIELD METHODS -------------------------

    @api.multi
    @api.depends('multi_date_due.date_due')
    def _compute_date_due(self):
        for record in self:
            first_date = None
            last_date = \
                datetime.strptime("9999-12-31 23:59:59", "%Y-%m-%d %H:%M:%S")
            if record.multi_date_due:
                for multi_date_due in record.multi_date_due:
                    if datetime.strptime(\
                        multi_date_due.date_due, "%Y-%m-%d") < last_date and \
                        multi_date_due.used == False:
                        first_date = multi_date_due.date_due
                    if multi_date_due.used == False:
                        last_date = datetime.strptime(\
                            multi_date_due.date_due, "%Y-%m-%d")
            else:
                first_date = None
            record.date_due = first_date

    @api.multi
    @api.depends('multi_date_due.date_due')
    def _compute_has_multi_date_due(self):
        for record in self:
            if record.multi_date_due:
                if len(record.multi_date_due) > 1:
                    record.has_multi_date_due = True
                else:
                    record.has_multi_date_due = False
